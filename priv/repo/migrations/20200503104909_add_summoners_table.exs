defmodule Spark.Repo.Migrations.AddSummonersTable do
  use Ecto.Migration

  def change do
    create table("summoners") do
      add :account_id, :string, null: false
      add :profile_icon_id, :integer, null: false
      add :revision_date, :naive_datetime, null: false
      add :name, :citext, null: false
      add :riot_id, :string, null: false
      add :puuid, :string, null: false
      add :summoner_level, :integer, null: false

      timestamps()
    end

    create index("summoners", [:name])
  end
end
