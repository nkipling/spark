defmodule SparkWeb.PageController do
  use SparkWeb, :controller

  def about(conn, _params) do
    render(conn, "about.html")
  end
end
