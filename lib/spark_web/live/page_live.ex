defmodule SparkWeb.PageLive do
  use SparkWeb, :live_view
  alias Spark.Summoner
  require Ecto.Query

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, query: "", results: %{})}
  end

  @impl true
  def handle_event("suggest", %{"q" => query}, socket) do
    {:noreply, assign(socket, results: %{}, query: query)}
  end

  @impl true
  def handle_event("search", %{"q" => query}, socket) do
    summoner =
      case Summoner |> Ecto.Query.where(name: ^query) |> Spark.Repo.one() do
        nil ->
          case Spark.Riot.api().summoner_details(query) do
            {:ok, summoner} ->
              summoner |> Spark.Repo.insert()

            _ ->
              nil
          end

        summoner ->
          summoner
      end

    if summoner == nil do
      {:noreply,
       socket
       |> put_flash(:error, "Summoner name not found")
       |> assign(results: %{}, query: query)}
    else
      {:noreply,
       push_redirect(
         socket,
         to: Routes.profile_path(socket, :index, summoner.name)
       )}
    end
  end
end
