defmodule SparkWeb.ProfileLive do
  use SparkWeb, :live_view
  require Ecto.Query

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, query: "", results: %{})}
  end

  @impl true
  def handle_params(%{"name" => name} = _params, _uri, socket) do
    IO.puts(name)

    {:noreply, assign(socket, :name, name)}
  end

  # @impl true
  # def handle_event("suggest", %{"q" => query}, socket) do
  #   {:noreply, assign(socket, results: %{}, query: query)}
  # end

  # @impl true
  # def handle_event("search", %{"q" => query}, socket) do
  # end
end
