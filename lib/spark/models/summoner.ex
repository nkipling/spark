defmodule Spark.Summoner do
  use Ecto.Schema

  @primary_key {:uuid, :binary_id, autogenerate: true}
  schema "summoners" do
    field :account_id, :string
    field :profile_icon_id, :integer
    field :revision_date, :naive_datetime
    field :name, :string
    field :riot_id, :string
    field :puuid, :string
    field :summoner_level, :integer

    timestamps()
  end

  def changeset(summoner, params \\ %{}) do
    summoner
    |> Ecto.Changeset.cast(params, [
      :account_id,
      :profile_icon_id,
      :revision_date,
      :name,
      :riot_id,
      :puuid,
      :summoner_level
    ])
    |> Ecto.Changeset.validate_required([
      :account_id,
      :profile_icon_id,
      :revision_date,
      :name,
      :riot_id,
      :puuid,
      :summoner_level
    ])
  end

  def from_json(data) when is_map(data) do
    %__MODULE__{}
    |> changeset(%{
      account_id: data["accountId"],
      profile_icon_id: data["profileIconId"],
      revision_date: DateTime.from_unix!(data["revisionDate"], :millisecond),
      name: data["name"],
      riot_id: data["id"],
      puuid: data["puuid"],
      summoner_level: data["summonerLevel"]
    })
    |> Ecto.Changeset.apply_changes()
  end
end
