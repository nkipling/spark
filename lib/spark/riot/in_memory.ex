defmodule Spark.Riot.InMemory do
  @behaviour Spark.Riot

  def summoner_details(name) do
    {:ok,
     %Spark.Summoner{
       account_id: "test-account",
       profile_icon_id: 1,
       revision_date: DateTime.now!("Etc/UTC"),
       name: name,
       riot_id: "test-id",
       puuid: "test-puuid",
       summoner_level: 30
     }}
  end
end
