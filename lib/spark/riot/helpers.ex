defmodule Spark.Riot.Helpers do
  def check_response(%{"status" => %{"status_code" => status_code}} = json_response) do
    case status_code do
      200 -> {:ok, json_response}
      _ -> {:error, status_code}
    end
  end

  def check_response(json_response) do
    {:ok, json_response}
  end

  def api_url(server) do
    "https://#{server}.api.riotgames.com/lol/"
  end
end
