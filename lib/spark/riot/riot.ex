defmodule Spark.Riot do
  @moduledoc """
  Defines the behaviour and how to access the Riot API.

  All access to the API should be through the `Spark.Riot.api/0` function, like
  so: `Spark.Riot.api()`. This ensures we are calling the correct implementation
  for each environment.
  """

  @doc """
  Gets the summoner details for the specified username

  Returns:
  * `{:ok, %Spark.Summopner{}}`
  * `{:error, error_msg}`

  ## Examples

      iex> Spark.Riot.api().summoner_details("nkrange")
      {:ok, %Spark.Summoner{}}
  """
  @callback summoner_details(summoner_name :: String.t()) :: {:ok, %Spark.Summoner{}}

  def api do
    Application.get_env(:spark, :riot_api)
  end
end
