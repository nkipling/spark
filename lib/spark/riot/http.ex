defmodule Spark.Riot.HTTP do
  @behaviour Spark.Riot

  use Tesla
  alias Spark.Riot.Helpers

  plug Tesla.Middleware.Headers, [{"X-Riot-Token", Application.get_env(:spark, :riot_key)}]
  plug Tesla.Middleware.JSON

  def summoner_details(name) do
    {:ok, response} = get("#{Helpers.api_url("euw1")}summoner/v4/summoners/by-name/" <> name)

    case Helpers.check_response(response.body) do
      {:ok, json} ->
        {:ok, json |> Spark.Summoner.from_json()}

      _ ->
        {:error, "Summoner not found"}
    end
  end
end
