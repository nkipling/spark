defmodule SparkRiotTest do
  use ExUnit.Case

  alias Spark.Riot
  alias Spark.Riot.Helpers

  describe "Spark.Riot.Helpers" do
    test "check_response returns :ok for normal response" do
      example_map = %{"response" => "should work"}
      result = Helpers.check_response(example_map)

      assert result == {:ok, example_map}
    end

    http_codes = [
      {200, :ok},
      {400, :error},
      {401, :error},
      {404, :error}
    ]

    for {code, response} <- http_codes do
      @code code
      @response response
      test "check_response returns #{response}' for code #{code}" do
        {result, _} = Helpers.check_response(%{"status" => %{"status_code" => @code}})

        assert result == @response
      end
    end

    test "api url contains supplied server" do
      server = "a-random-test-server-that-cannot-possibly-exist"
      result = Helpers.api_url(server)

      assert result =~ server
    end
  end

  test "returns a summoner for the specified name" do
    name = "test-name"
    {response, summoner} = Riot.api().summoner_details(name)

    assert response == :ok
    assert summoner.name == name
  end
end
