defmodule SparkWeb.PageLiveTest do
  use SparkWeb.ConnCase

  import Phoenix.LiveViewTest

  test "disconnected and connected render", %{conn: conn} do
    {:ok, page_live, disconnected_html} = live(conn, "/")
    assert disconnected_html =~ "Improve your game with Spark"
    assert render(page_live) =~ "Improve your game with Spark"
  end
end
