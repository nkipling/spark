# Spark

A small application built using Elixir Phoenix with Live View for gathering your
League of Legends stats. This is mostly a project for me to try out the Live View
functionality and explore how this works. Mostly a learning / dev exercise.

### Todos

* Add CI/CD
* Create hosting environment
* Add URL to readme

## Getting Started

If you want to try this out locally, you will need to first run `mix setup`. You
will need a Postgres Db and a [Riot Games API key](https://developer.riotgames.com/).
After running setup, create a new file called `config.secret.exs` inside the
`config` directory. Add the following content:

```elixir
use Mix.Config

config :spark,
  ecto_repos: [Spark.Repo],
  riot_key: "your-api-key"
```

Once you've done this, you can run `mix phx.server` to start the application. Or
you can run `iex -S mix phx.server` if you want the interactive console.

Now you can visit [`localhost:9999`](http://localhost:9999) from your browser.
