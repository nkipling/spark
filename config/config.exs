# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :spark,
  ecto_repos: [Spark.Repo]

# Configures the endpoint
config :spark, SparkWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "pdHK/j5c1k3VekXIiRPzwpoFqaHThj4Od+jgzTYiOWYmZUDFhtjdzbtRfYRY7cGm",
  render_errors: [view: SparkWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Spark.PubSub,
  live_view: [signing_salt: "g+LeXAu6"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
import_config "config.secret.exs"
